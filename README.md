# Proyecto de Modela 2.

Each service has its own directory, with a graphs subdirectory holding the images for the fitting
tests. A file named $SERVICE.log is located within each subdirectory. At the bottom of such file
is the conclusion of which distribution fits best and the estimated parameters.

# Diagram
Based on this information, the last section of the report is the diagram. It won't be diagramed on
Simio, we can use any diagraming tool. Draw.io sounds just fine. A simple enough croquis that 
shows the order of the activities, the relationships between them and the places (exam room,
triage room, procedure room, imaging room, etc) as well as the services is just fine.
