-- with waiting AS (select * from activity_log where actividad in (35) order by paciente_id, hora)
-- select 
--     (extract(seconds from al.hora - w.hora) +
--     extract(minutes from al.hora - w.hora)*60 +
--     extract(hours from al.hora - w.hora)*60*60)/60 espera
-- from waiting w inner join activity_log al ON w.id = al.id - 1
-- inner join actividad a ON a.id = w.actividad inner join actividad aa ON aa.id = al.actividad
-- where aa.id = 20
-- order by w.paciente_id asc, w.hora asc;

-- 6385
-- 4.8238057948316364
select count(*) from activity_log 
where actividad in (35,27,31,40);
-- with waiting AS (select * from activity_log order by paciente_id, hora)
-- select a.nombre actividad_inicial from
--     waiting w inner join activity_log al ON w.id = al.id - 1
-- inner join actividad a ON a.id = w.actividad inner join actividad aa ON aa.id = al.actividad
-- where aa.id = 20
-- group by a.nombre;
